package drawing_app_client.view;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javax.imageio.ImageIO;

import drawing_app_client.ClientMainApp;
//import drawing_app_client.model.DrawingCoordinates;
import drawing_coordinates.*;



import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class DrawingWindowController implements Initializable{

	private ClientMainApp mainApp;
	private Scene scene;
	private Stage stage;
	private Color color;
	GraphicsContext gc;
	private DrawingCoordinates coordinates;

	@FXML 
	Canvas canvas;

	@FXML
	ColorPicker colorpicker;
	

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		gc = canvas.getGraphicsContext2D();
		gc.setStroke(Color.BLACK);
		gc.setLineWidth(15);

	}

	
	public void setMainApp (ClientMainApp mainApp) {
		this.mainApp = mainApp;
	}

	public void setScene (Scene scene) {
		this.scene = scene;
	}

	public void setStage(Stage stage) {
		this.stage = stage;
	}

	public void setMouseEvents () {	

		EventHandler<MouseEvent> mouseHandler = new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {


				if (event.getEventType() == MouseEvent.MOUSE_PRESSED) {

					double x = event.getSceneX();
					double y = event.getSceneY();

					gc.beginPath();
					gc.lineTo(x, y);
					gc.stroke();

					//setting up the coordinates object to send
					coordinates = new DrawingCoordinates(x, y);	
					coordinates.setState(DrawingCoordinates.STATE_PRESSED);
					coordinates.setColor(colorpicker.getValue());

					mainApp.sendDrawingInstructionsToServer(coordinates);

				} else if (event.getEventType() == MouseEvent.MOUSE_DRAGGED) {

					double x = event.getSceneX();
					double y = event.getSceneY();

					gc.lineTo(event.getSceneX(), event.getSceneY());
					gc.stroke();	

					//setting up the coordinates object to send

					coordinates = new DrawingCoordinates(x, y);
					coordinates.setState(DrawingCoordinates.STATE_DRAGGED);
					coordinates.setColor(colorpicker.getValue());


					System.out.println("Color from colorpicker: " + colorpicker.getValue());


					mainApp.sendDrawingInstructionsToServer(coordinates);

					System.out.println("x =  " +event.getSceneX() + "   y = "  + event.getSceneY() + "/n");

				}


			}

		};

		scene.setOnMousePressed(mouseHandler);
		scene.setOnMouseDragged(mouseHandler);
	}

	public void setColorPicker() {
		colorpicker.getStyleClass().add("split-button");
		colorpicker.setValue(Color.BLACK);

		colorpicker.setOnAction(new EventHandler<ActionEvent> () {

			@Override
			public void handle(ActionEvent event) {
				color = new Color(0, 0, 0, 0);
				color = colorpicker.getValue();
				gc.setStroke(color);

			}
		});

	}


	@FXML
	private void saveHandle( ) {

		FileChooser fc = new FileChooser();
		FileChooser.ExtensionFilter ext_filter = new FileChooser.ExtensionFilter("png files (*.png)", "*.png");
		fc.getExtensionFilters().add(ext_filter);

		File destination_file = fc.showSaveDialog(stage);

		if(destination_file != null) {
			try {
				WritableImage wi = new WritableImage((int)canvas.getWidth(), (int)canvas.getHeight());
				canvas.snapshot(null,wi);
				RenderedImage ri = SwingFXUtils.fromFXImage(wi, null);
				ImageIO.write(ri, "png", destination_file);

			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}


	public void drawCoordinatesFromAnotherClient(DrawingCoordinates coords) {
		Paint previous = gc.getStroke();
		System.out.println("drawCoords was CALLEEEED");
		if (coords.getState() == DrawingCoordinates.STATE_PRESSED) {
			gc.beginPath();
			gc.lineTo(coords.getX(), coords.getY());
			gc.setStroke(coords.getColor());
			gc.stroke();

		} else if (coords.getState() == DrawingCoordinates.STATE_DRAGGED){
			gc.lineTo(coords.getX(), coords.getY());
			gc.setStroke(coords.getColor());
			gc.stroke();			
		}
		gc.setStroke(previous);

	}
}