package drawing_app_client;
import java.io.IOException;

//import drawing_app_client.model.DrawingCoordinates;
import drawing_coordinates.*;

import drawing_app_client.view.DrawingWindowController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;


/* TODO: Learn more about exception handling 
         Figure out how to disconnect a client who clicks x on the canvas
         add Close button to the canvas
         add Color choosing to the canvas
         */


public class ClientMainApp extends Application {

	public MyClient client = null;
	public DrawingWindowController controller = null;

	public void start(Stage primaryStage) throws Exception {


		// setting up the GUI
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("view/DrawingWindow.fxml"));
		AnchorPane root = loader.load();

		Scene scene = new Scene(root, 600, 400);
		controller = loader.getController();

		//setting up the scene from the ChatWindow Controller
		setupController(primaryStage, scene);
		

		//connecting to the server and setting up the client class
		try {
			int port = 2001;
			String address = "192.168.0.15";
			client = new MyClient(address, port);
			client.createThreadforReceivingDrawingInstructions();
			client.setMainApp(this);
			client.setWindowController(controller);

		} catch (IOException e) {
			e.printStackTrace();

		}
		
		//show the stage
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	private void setupController (Stage primaryStage, Scene scene) {
		controller.setMainApp(this);
		controller.setStage(primaryStage);
		controller.setScene(scene);
		controller.setMouseEvents();
		controller.setColorPicker();
	}

	//sending drawing instructions to client to send it to the Server which will distribute them to all clients 
	public void sendDrawingInstructionsToServer (DrawingCoordinates coords) {
		try {
			
			client.sendingDrawingInstructionsToServer(coords);

		} catch (NullPointerException e) {

			System.out.println("NO SERVER TO CONNECT TO!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
		}
	}

	//sending RECEIVED drawing instructions to controller
	public void sendDrawingInstructionstoControllerToDraw(DrawingCoordinates coords) {

		controller.drawCoordinatesFromAnotherClient(coords);
	}

	public static void main (String[] arg) {

		launch(arg);
	}


}

