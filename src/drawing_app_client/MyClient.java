package drawing_app_client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

//import drawing_app_client.model.DrawingCoordinates;
import drawing_app_client.view.DrawingWindowController;
import drawing_coordinates.*;


public class MyClient {


	private Socket my_socket = null;
	private ObjectInputStream cin_stream = null;
	private ObjectOutputStream cout_stream = null;
	private Thread receive_message = null;
	private ClientMainApp mainApp;

	public MyClient (String address, int port) throws IOException {

		my_socket = new Socket(address, port);
		cout_stream = new ObjectOutputStream(my_socket.getOutputStream());
		cin_stream = new ObjectInputStream(my_socket.getInputStream());
	}

	// method for sending canvas coordinates for drawing
	public void sendingDrawingInstructionsToServer (DrawingCoordinates coords) {
			try {
				cout_stream.writeObject(coords);
				cout_stream.flush();

			} catch (IOException e) {
				System.out.println("COuldn't write to the outputstream stream!!!!!");
			}

	}

	// establishing thread for receiving messages
	public void createThreadforReceivingDrawingInstructions () {
		receive_message = new Thread(new Runnable() {

			public void run() {

				try {

					while (true) {

						DrawingCoordinates coords_from_another_client = (DrawingCoordinates)cin_stream.readObject();

						System.out.println(" X Coordinate received: " + coords_from_another_client.getX() + "\n");
						System.out.println(" Y Coordinate received: " + coords_from_another_client.getY() + "\n");

						mainApp.sendDrawingInstructionstoControllerToDraw(coords_from_another_client);

					} 
					
				}catch (ClassNotFoundException | IOException e) {

					System.out.println("Didn't read anything");
					e.printStackTrace();
				}
			}
		}

				);

		receive_message.start();
	}

	public void setWindowController (DrawingWindowController controller) {
	}
	
	public void setMainApp (ClientMainApp mainApp) {
		this.mainApp = mainApp;
	}

	public void closeConnection () throws IOException {
		System.out.println("Connection closed for this thread: ");

		System.out.println("Closing server_response_scanner... ");

		System.out.println("Closing cin_stream... ");

		cin_stream.close();

		System.out.println("Closing cout_stream... ");

		cout_stream.close();

		System.out.println("Closing my_socket... ");

		my_socket.close();

		System.out.println("Closing user_input_scanner... ");

	}
}




